<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('comprador_id')->unsigned();
            $table->string('comprador_telefono');
            $table->string('comprador_calle');
            $table->string('comprador_numero_interior');
            $table->string('comprador_colonia');
            $table->string('comprador_codigo_postal');
            $table->string('comprador_ciudad');
            $table->string('comprador_estado');
            $table->string('comprador_pais');
            $table->timestamps();
        });
        Schema::table('ventas_articulos', function(Blueprint $table) {
          $table->foreign('venta_id')->references('id')
          ->on('ventas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventas');
    }
}
