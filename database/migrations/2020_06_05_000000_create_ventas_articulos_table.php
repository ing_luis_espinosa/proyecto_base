<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVentasArticulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventas_articulos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('articulo_id')->unsigned();
            $table->foreignId('venta_id')->unsigned();
            $table->integer('cantidad');
            $table->float('precio', 8, 2);
            $table->float('porcentaje_iva', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventas_articulos');
    }
}
