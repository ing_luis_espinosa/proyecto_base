<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('titulo');
            $table->string('descripcion');
            $table->float('precio', 8, 2);
            $table->integer('existencias');
            $table->foreignId('categoria_id')->unsigned();
            $table->string('imagen');
            $table->timestamps();
        });
        Schema::table('ventas_articulos', function(Blueprint $table) {
          $table->foreign('articulo_id')->references('id')
          ->on('articulos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articulos');
    }
}
