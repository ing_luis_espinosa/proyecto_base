<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ventas_articulo extends Model
{
  protected $table = 'Ventas_articulos';
  public $timestamps = true;
  protected $fillable = ['articulo_id', 'venta_id', 'cantidad',
  'precio', 'procentaje_iva'];
  public function articulo() {
      return $this->belongsTo(Articulo::class);
  }
  public function venta() {
      return $this->belongsTo(Venta::class);
  }
}
