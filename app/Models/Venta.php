<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
  protected $table = 'ventas';
  public $timestamps = true;
  protected $fillable = ['comprador_id', 'comprador_telefono', 'comprador_calle',
  'comprador_numero', 'comprador_colonia', 'comprador_codigo_postal',
  'comprador_ciudad', 'comprador_estado', 'comprador_pais'];
  public function comprador() {
      return $this->belongsTo(User::class);
  }
}
