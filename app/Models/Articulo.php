<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Articulo extends Model
{
 protected $table = 'articulos';
 public $timestamps = true;
 protected $fillable = ['titulo', 'descripcion', 'precio',
 'existencias', 'categoria_id', 'imagen'];
 public function categoria() {
     return $this->belongsTo(Categoria::class);
 }
}
