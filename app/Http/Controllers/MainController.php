<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Articulo;
use App\Models\Categoria;

class MainController extends Controller
{
  public function index()
  {
      $articulos = Articulo::all();
      $categorias = Categoria::all();
      return Inertia::render('main/index', [
        "articulos" => $articulos,
        "categorias" => $categorias,
        "mensaje" => "hola"
      ]);
  }
  public function agregarArticulo(Request $request)
  {
    $articulo=new Articulo;
    $articulo->titulo = $request->titulo;
    $articulo->descripcion = $request->descripcion;
    $articulo->precio = $request->precio;
    $articulo->existencias = $request->existencias;
    $articulo->categoria_id = $request->categoria_id;
    $articulo->imagen = $request->imagen;
    $articulo->save();
    return $this->index();
  }
}
